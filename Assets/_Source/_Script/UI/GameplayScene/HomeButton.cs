﻿using System.Threading.Tasks;

public class HomeButton : IGameplayButton
{
    public async void Activate()
    {
        DarkZoneEffect.Instance.Close();
        await SceneLoadingMachine.LoadScene("Home", true);
        DarkZoneEffect.Instance.Open();
    }
}