﻿using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using DG.Tweening;

public class RetryButton : IGameplayButton
{
    public async void Activate()
    {
        var sceneInstance = await SceneLoadingMachine.LoadScene("Main", false);
        DarkZoneEffect.Instance.Close().OnComplete(openScene);

        return;

        async void openScene()
        {
            await sceneInstance.ActivateAsync();
            await UniTask.Delay(TimeSpan.FromSeconds(.5f));
            DarkZoneEffect.Instance.Open();
        }
    }
}