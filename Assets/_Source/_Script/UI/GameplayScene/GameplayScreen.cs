﻿using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameplayScreen : MonoBehaviour
{
    [SerializeField] private Button homeBtn;
    [SerializeField] private Button restartBtn;
    [SerializeField] private Button skipBtn;

    [SerializeField] private TextMeshProUGUI levelText;
    
    private void Start()
    {
        homeBtn.onClick.AddListener(OnHomeBtnClick);
        restartBtn.onClick.AddListener(OnRestartBtnClick);
        skipBtn.onClick.AddListener(OnSkipBtnClick);
    }

    public void OnEnable()
    {
        levelText.text = $"LEVEL {MemoryAccess.GetCurrentLevel()}";
    }

    private void OnSkipBtnClick()
    {
        // TODO: SHOW REWARD
        
    }

    private async void OnRestartBtnClick()
    {
        var sceneInstance = await SceneLoadingMachine.LoadScene("Main", false);
        DarkZoneEffect.Instance.Close().OnComplete(openScene);

        return;
        async void openScene()
        {
            await sceneInstance.ActivateAsync();
            await UniTask.Delay(TimeSpan.FromSeconds(.5f));
            DarkZoneEffect.Instance.Open();
        }
    }

    private async void OnHomeBtnClick()
    {
        DarkZoneEffect.Instance.Close();
        await SceneLoadingMachine.LoadScene("Home", true);
        DarkZoneEffect.Instance.Open();
    }
}

public interface IGameplayButton
{
    void Activate();
}