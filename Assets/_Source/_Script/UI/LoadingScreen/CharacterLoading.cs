﻿using System;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using UnityEngine;

    public class CharacterLoading : MonoBehaviour, MMEventListener<MMCharacterEventTypes>
    {
        public void OnMMEvent(MMCharacterEventTypes eventType)
        {
            Debug.Log("Event received: " + eventType);
        }
    }
