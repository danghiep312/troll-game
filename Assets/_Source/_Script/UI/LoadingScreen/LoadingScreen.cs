﻿using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using MoreMountains.CorgiEngine;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;


public class LoadingScreen : MonoBehaviour
{
    public Character character;
    public DarkZoneEffect effect;

    public TMP_Text text;
    public float delayBetween = .3f;
    public float delayToClose = 1f;

    private WaitForSeconds _delay;

    public string nextScene;

    private async void Start()
    {
        _delay = new WaitForSeconds(delayBetween);
        

        AsyncOperationHandle<SceneInstance> nextScene =
            Addressables.LoadSceneAsync(this.nextScene, LoadSceneMode.Single, false);
        
        
        Util.Delay(delayToClose, () =>
        {
            CloseAnimation().OnComplete(openScene);
        });

        return;

        async void openScene()
        {
            await nextScene.Task;
            await UniTask.Delay(TimeSpan.FromSeconds(1f));
            await nextScene.Result.ActivateAsync();

            effect.Open();
        }
    }

    private void Update()
    {
        effect.SetPositionMask(character.transform.position + Vector3.up * .5f);

        if (text.text.Equals("Loading"))
        {
            StartCoroutine(TextLoading());
        }
    }

    private Sequence CloseAnimation()
    {
        return effect.Close(transform.position, new DarkZoneSetting
        {
            InitValue = Vector3.one * 2f,
            Duration = .2f,
            Ease = Ease.Linear,
            Delay = 0f,
            Target = Vector3.zero
        });
    }


    private IEnumerator TextLoading()
    {
        yield return _delay;
        text.text = "Loading.";
        yield return _delay;
        text.text = "Loading..";
        yield return _delay;
        text.text = "Loading...";
        yield return _delay;
        text.text = "Loading";
    }
}