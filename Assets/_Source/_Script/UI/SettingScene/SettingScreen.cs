﻿using System;
using DG.Tweening;
using Framework.DesignPattern.Observer;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SettingScreen : MonoBehaviour
{
    [SerializeField] private Button homeBtn;
    [SerializeField] private Button resetProgressBtn;

    private SceneInstance _popupScene;
    
    private void Start()
    {
        homeBtn.onClick.AddListener(OnHomeBtnClick);
        resetProgressBtn.onClick.AddListener(OnResetProgressBtnClick);
        
        this.RegisterListener(EventID.CloseNoticePopup, OnCloseNoticePopup);
    }

    private void OnHomeBtnClick()
    {
        DarkZoneEffect.Instance.Close().OnComplete(GoToHome);
    }

    private async void GoToHome()
    {
        await SceneLoadingMachine.LoadScene("Home", true);
        DarkZoneEffect.Instance.Open();
    }

    private async void OpenResetPopup()
    {
        AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync("Popup", LoadSceneMode.Additive, true);
        await handle.Task;
        _popupScene = handle.Result;
    }
    

    private void OnResetProgressBtnClick()
    {
        OpenResetPopup();
    }
    
    private void OnCloseNoticePopup(object obj)
    {
        Addressables.UnloadSceneAsync(_popupScene);
    }
}