﻿using System;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RetryScreen : MonoBehaviour
{
    [SerializeField] private Button retryButton;
    [SerializeField] private TextMeshProUGUI lifeText;

    private void Start()
    {
        UpdateLife();
        retryButton.onClick.AddListener(RetryLevel);
    }

    private void UpdateLife()
    {
        lifeText.text = LifeManager.Life.ToString();
    }

    public async void RetryLevel()
    {
        await SceneLoadingMachine.LoadScene("Main", true);
        await UniTask.Delay(TimeSpan.FromSeconds(0.5f));
        
        DarkZoneEffect.Instance.Open();
        
    }
}