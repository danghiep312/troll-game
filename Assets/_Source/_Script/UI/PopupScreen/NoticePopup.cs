﻿using System;
using Framework.DesignPattern.Observer;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NoticePopup : MonoBehaviour
{
    [SerializeField] private Button yesBtn;
    [SerializeField] private Button noBtn;

    public void Start()
    {
        yesBtn.onClick.AddListener(OnYesBtnClick);
        noBtn.onClick.AddListener(OnNoBtnClick);
    }

    private void OnNoBtnClick()
    {
        Close();
    }

    private void OnYesBtnClick()
    {
        MemoryAccess.SetCurrentLevel(1);
        Close();
    }

    private void Close()
    {
        this.PostEvent(EventID.CloseNoticePopup);
    }
}