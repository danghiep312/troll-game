﻿using System;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.UI;

public class ButtonVibration : MonoBehaviour
{
    [SerializeField] private HapticTypes _hapticTypes;

    public void Start()
    {
        GetComponent<Button>().onClick.AddListener(Play);
    }

    public void Play()
    {
        if (!MemoryAccess.GetBoolStatus(MemoryKey.VIBRATE)) return;
        MMVibrationManager.Haptic(_hapticTypes);
    }
}