﻿using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HomeScreen : MonoBehaviour
{
    [SerializeField] private Button settingBtn;
    [SerializeField] private Button playBtn;

    private void Start()
    {
        settingBtn.onClick.AddListener(OnSettingBtnClick);
        playBtn.onClick.AddListener(OnPlayBtnClick);
    }

    private async void OnPlayBtnClick()
    {
        var instance = await SceneLoadingMachine.LoadScene("Main", false);
        DarkZoneEffect.Instance.Close().OnComplete(openScene);
        
        return;
        async void openScene()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(0.5f));
            await instance.ActivateAsync();
            DarkZoneEffect.Instance.Open();
        }

    }

    private void OnSettingBtnClick()
    {
        SceneLoadingMachine.LoadScene("Setting", true);
    }
}