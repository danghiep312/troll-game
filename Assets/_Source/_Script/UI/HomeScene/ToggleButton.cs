﻿using System;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ToggleButton : MonoBehaviour
{   
    public static List<MemoryKey> AudioKey = new () {MemoryKey.BGM, MemoryKey.SFX};
    
    public MemoryKey key;

    public Toggle toggle;

    public List<GameObject> status;

    private void OnEnable()
    {
        toggle.isOn = MemoryAccess.GetBoolStatus(key);
        UpdateSprite();
    }

    private void Start()
    {
        toggle.onValueChanged.AddListener(OnToggle);
    }

    private void OnToggle(bool isOn)
    {
        MemoryAccess.SetBoolStatus(key, isOn);
        OnAudioKey(isOn);
        UpdateSprite();
    }

    private void OnAudioKey(bool isOn)
    {
        if (!AudioKey.Contains(key)) return;
        switch (key)
        {
            case MemoryKey.SFX:
                AudioController.SetSfx(isOn);
                break;
            case MemoryKey.BGM:
                AudioController.SetBgm(isOn);
                break;
        }
    }

    private void UpdateSprite()
    {
        foreach (var obj in status)
        {
            obj.SetActive(false);
        }
        
        status[toggle.isOn ? 1 : 0].SetActive(true);
    }

    private void OnValidate()
    {
        if (!toggle)
        {
            toggle = GetComponentInChildren<Toggle>();
        }
    }
}