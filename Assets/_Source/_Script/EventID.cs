﻿
    public enum EventID
    {
        None = 0,
        CloseLoading = 1,
        ClosePanel = 2,
        CloseNoticePopup = 3,
    }