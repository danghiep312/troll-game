﻿using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfig", menuName = "SO/GameConfig")]
public class GameConfig : ScriptableObject
{
    public SerializableDictionaryBase<ConfigType, ConfigValue> config;
    
    
    public ConfigValue GetConfig(ConfigType type)
    {
        return config[type];
    }
}

public enum ConfigType
{
    NONE = 0,
    DEFAULT_LIFE = 1,
}

[Serializable]
public class ConfigValue
{
    
}

[Serializable]
public class IntValue : ConfigValue
{
    public int Value;
    public int GetValue() => Value;
}

[Serializable]
public class FloatValue : ConfigValue
{
    public float Value;
    public float GetValue() => Value;
}

[Serializable]
public class StringValue : ConfigValue
{
    public string Value;
    public string GetValue() => Value;
}

