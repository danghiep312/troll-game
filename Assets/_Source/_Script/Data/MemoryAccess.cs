﻿using System;
using BansheeGz.BGDatabase;
using Cysharp.Threading.Tasks;
using Framework.DesignPattern.Singleton;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class MemoryAccess
{
    public static GameData GameConfig = GameData.GetEntity(0);

    // public static void Fetch()
    // {
    //     GameConfig = GameData.GetEntity(0);
    //     // GameConfig = (await Addressables.LoadAssetAsync<GameConfig>("GameConfig").ToUniTask());
    // }
    
    static string GetKey(MemoryKey key)
    {
        return key.ToString();
    }

    public static int GetCurrentLevel()
    {
        return PlayerPrefs.GetInt(GetKey(MemoryKey.CURRENT_LEVEL), 1);
    }

    public static int SetCurrentLevel(int level)
    {
        PlayerPrefs.SetInt(GetKey(MemoryKey.CURRENT_LEVEL), level);
        return level;
    }

    public static bool GetBoolStatus(MemoryKey key)
    {
        return PlayerPrefs.GetInt(GetKey(key), 1) == 1;
    }

    public static void SetBoolStatus(MemoryKey key, bool value)
    {
        PlayerPrefs.SetInt(key.ToString(), value ? 1 : 0);
    }

    public static int GetLife()
    {
        return PlayerPrefs.GetInt(GetKey(MemoryKey.LIFE), GameConfig.life);
    }
    
    public static void SetLife(int life)
    {
        PlayerPrefs.SetInt(GetKey(MemoryKey.LIFE), life);
    }
}

public enum MemoryKey
{
    NONE = 0,
    CURRENT_LEVEL = 1,
    SFX = 2, 
    BGM = 3,
    VIBRATE = 4,
    SHOW_CONTROL = 5,
    LIFE = 6,
}