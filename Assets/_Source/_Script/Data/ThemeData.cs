using UnityEngine;
using UnityEngine.Serialization;


[CreateAssetMenu(fileName = "ThemeData", menuName = "SO/Theme Data")]
	public class ThemeData : ScriptableObject
	{
		[FormerlySerializedAs("IndexLevel")] public int indexLevel;

		[FormerlySerializedAs("_tileMapColor")] [SerializeField]
		public Color tileMapColor;

		[FormerlySerializedAs("_backgroundColor")] [SerializeField]
		public Color backgroundColor;

		[FormerlySerializedAs("_sawColor")] [SerializeField]
		public Color sawColor;
		
	}
