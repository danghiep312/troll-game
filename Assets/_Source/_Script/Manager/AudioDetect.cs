﻿using MoreMountains.Tools;
using Sirenix.OdinInspector;
using UnityEngine;

public class AudioDetect : MonoBehaviour
{
    private void Start()
    {
        CheckSfx();
        CheckBgm();
    }

    private void CheckSfx()
    {
        AudioController.SetSfx(MemoryAccess.GetBoolStatus(MemoryKey.SFX));
    }

    private void CheckBgm()
    {
        AudioController.SetBgm(MemoryAccess.GetBoolStatus(MemoryKey.BGM));
    }

    [Button]
    public void Set(bool sfx, bool value)
    {
        if (sfx)
        {
            AudioController.SetSfx(value);
        }
        else
        {
            AudioController.SetBgm(value);
        }
    }
    
}

public class AudioController
{
    public static void SetSfx(bool value)
    {
        Common.Log("Set Sfx: " + value);
        if (value)
        {
            MMSoundManager.Instance.UnmuteSfx();
        }
        else
        {
            MMSoundManager.Instance.MuteSfx();
        }
    }
    
    public static void SetBgm(bool value)
    {
        Common.Log("Set Bgm: " + value);
        if (value)
        {
            MMSoundManager.Instance.UnmuteMusic();
        }
        else
        {
            MMSoundManager.Instance.MuteMusic();
        }
    }
}