﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Framework.DesignPattern.Singleton;
using UnityEngine;
using UnityEngine.AddressableAssets;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

public class EffectManager : Singleton<EffectManager>
{
    public SerializableDictionaryBase<EffectType, AssetReference> effectMappingRef;
    public SerializableDictionaryBase<EffectType, List<EffectObject>> pools;


    private void Start()
    {
        pools = new SerializableDictionaryBase<EffectType, List<EffectObject>>();
        foreach (string key in Enum.GetNames(typeof(EffectType)))
        {
            pools.Add(key.ParseEnum<EffectType>(), new List<EffectObject>());
        }
    }

    public EffectObject Spawn(EffectType effectType)
    {
        AssetReference assetReference = GetReference(effectType);
        // Assert.IsNotNull(assetReference);

        var effectObject = ObjectPool.GetPool(assetReference).Take(transform);
        return effectObject.GetComponent<EffectObject>();
    }

    public AssetReference GetReference(EffectType effectType)
    {
        if (effectMappingRef.TryGetValue(effectType, out var assetReference))
        {
            return assetReference;
        }

        Common.LogWarning("Reference not found for effect type: " + effectType, Instance);
        return null;
    }

    public void PlayEffect(EffectType effectType)
    {
        GetEffect(effectType).PlayEffect(true);
    }
    
    public void PlayEffect(EffectType effectType, Vector3 position)
    {
        GetEffect(effectType).PlayEffect(position, true);
    }

    private EffectObject GetEffect(EffectType effectType)
    {
        EffectObject effectObject = pools[effectType].GetEffectIsAvailable();

        if (effectObject == null)
        {
            effectObject = Spawn(effectType);
            pools[effectType].Add(effectObject);
        }

        return effectObject;
    }

    private void OnValidate()
    {
        foreach (var key in Enum.GetNames(typeof(EffectType)))
        {
            if (key.Equals(EffectType.NONE.ToString())) continue;
            if (effectMappingRef.ContainsKey(key.ParseEnum<EffectType>())) continue;
            Common.LogWarning("Assign reference for effect type: " + key, this);
            effectMappingRef.TryAdd(key.ParseEnum<EffectType>(), null);
        }
    }
}

public static class EffectManagerExtension
{
    public static EffectObject GetEffectIsAvailable(this List<EffectObject> list)
    {
        return list.FirstOrDefault(effectObject => !effectObject.IsPlaying());
    }
}

