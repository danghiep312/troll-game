﻿
    using UnityEngine;

    public abstract class EffectObject : MonoBehaviour
    {
        public EffectType effectType;

        public abstract bool IsPlaying();
        
        public abstract void PlayEffect(bool includeChildren = false);
        
        
        public abstract void StopEffect(bool includeChildren = false);
        
        public abstract void SetPosition(Vector3 position);

        public void PlayEffect(Vector3 position, bool includeChildren = false)
        {
            SetPosition(position);
            PlayEffect(includeChildren);
        }
    }
    