﻿using System;
using UnityEngine;

public class ParticleEffectObject : EffectObject
{
    public ParticleSystem ps;

    public override bool IsPlaying()
    {
        return ps.isPlaying;
    }
    
    public override void PlayEffect(bool includeChildren = false)
    {
        ps.Play(includeChildren);
    }

    public override void StopEffect(bool includeChildren = false)
    {
        ps.Stop(includeChildren);
    }

    public override void SetPosition(Vector3 position)
    {
        ps.transform.position = position;
    }

    private void OnValidate()
    {
        if (ps == null)
        {
            ps = GetComponentInChildren<ParticleSystem>();
        }
    }
}