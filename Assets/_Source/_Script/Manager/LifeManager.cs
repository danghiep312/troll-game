﻿public class LifeManager
{
    public static int Life;

    public static void Fetch()
    {
        Life = MemoryAccess.GetLife();
    }

    public static void UseLife()
    {
        Common.Log("Use Life");
        Life--;
        SaveLife();
    }

    public static void SaveLife()
    {
        MemoryAccess.SetLife(Life);
    }
}