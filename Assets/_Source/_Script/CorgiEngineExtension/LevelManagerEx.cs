﻿using System;
using MoreMountains.CorgiEngine;
using UnityEngine;


    public class LevelManagerEx : LevelManager
    {
        public Goal goal;
        
        public override void OnMMEvent(CorgiEngineEvent engineEvent)
        {
        }

        private void OnValidate()
        {
            if (goal == null)
            {
                goal = transform.parent.GetComponentInChildren<Goal>();
            }
        }
    }
