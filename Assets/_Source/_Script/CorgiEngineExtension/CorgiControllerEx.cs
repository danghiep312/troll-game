﻿using System;
using MoreMountains.CorgiEngine;
using UnityEngine;


public class CorgiControllerEx : CorgiController
{
    [SerializeField] private Collider2D _collider;
    public void SetColliderActive(bool status)
    {
        _collider.enabled = status;
    }

    private void OnValidate()
    {
        if (!_collider)
            _collider = GetComponent<Collider2D>();
    }
    
    public void ReachedEndTeleport()
    {
        GetComponent<Character>().UnFreeze();
        GetComponent<CharacterScaleChanger>().BeginScale(Vector3.one, .3f, false);
        SetColliderActive(true);
    }
}