﻿using System;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

public class TestFunc : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("TestFunc.OnTriggerEnter2D " + gameObject.name + " " + other.gameObject.tag);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("TestFunc.OnTriggerEnter " + gameObject.name);
    }

    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("TestFunc.OnCollisionEnter " + gameObject.name);
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("TestFunc.OnCollisionEnter2D " + gameObject.name);
    }

    public void LogTest()
    {
        Debug.Log("TestFunc.Log");
    }

    [Button]
    public virtual void Test1()
    {
        Debug.Log("Test 1 par");
        Test2();
    }
    
    public virtual void Test2()
    {
        Debug.Log("Test 2 par");
    }
    
    public FallingPlatformEx fallingPlatformEx;

    [Button]
    public void Falling()
    {
        fallingPlatformEx.StartShaking();
    }
}