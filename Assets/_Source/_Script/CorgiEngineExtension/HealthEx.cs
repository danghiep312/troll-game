﻿using MoreMountains.CorgiEngine;
using SingularityGroup.HotReload;
using UnityEngine;


public class HealthEx : Health
    {
        public override void Kill()
        {
            base.Kill();
            Debug.Log("Kill");
            EffectManager.Instance.PlayEffect(EffectType.EXPLOSION, transform.position);
        }
    }
