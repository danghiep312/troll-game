﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using Unity.Mathematics;
using UnityEngine;

public class CharacterScaleChanger : CharacterAbility
{
    public enum Phase
    {
        Idle = 0,
        Scaling = 1
    }

    private Vector3 _beginScale;

    private Vector3 _targetScale;

    private float _changeDuration;

    private float _changeTimer;

    private Phase _phase;

    private string _sfxScaleUp;

    private string _sfxScaleDown;

    [SerializeField] private Transform scaleCenter;

    protected override void Initialization()
    {
        base.Initialization();
        _phase = Phase.Idle;
    }

    /// <param name="targetScale"></param>
    /// <param name="duration"></param>
    /// <param name="relativeToCurrent">If true, scale the target object relative to current size instead of original size</param>
    public void BeginScale(Vector3 targetScale, float duration, bool relativeToCurrent)
    {
        _targetScale = targetScale;
        _changeDuration = duration;
        _changeTimer = 0f;
        _beginScale = scaleCenter.localScale;

        _phase = Phase.Scaling;
    }

    public override void ProcessAbility()
    {
        if (_phase == Phase.Scaling)
        {
            var distance = _targetScale - _beginScale;

            _changeTimer += Time.deltaTime;
            float remap = MMMaths.Remap(_changeTimer, 0, _changeDuration, 0f, 1f);
            float factor = math.min(MMTween.Evaluate(remap, MMTween.MMTweenCurve.LinearTween), 1);
            scaleCenter.localScale = _beginScale + distance * factor;
            
            if (scaleCenter.localScale == _targetScale)
            {
                ReachedEnd();
            }
        }
    }


    private void ReachedEnd()
    {
        _phase = Phase.Idle;
    }
}