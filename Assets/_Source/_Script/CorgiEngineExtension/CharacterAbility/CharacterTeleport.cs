﻿using System;
using DG.Tweening;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using UnityEngine;

public class CharacterTeleport : CharacterAbility
{
    public enum Phase
    {
        Idle = 0,
        Teleporting = 1,
        InstantTeleporting = 2
    }

    [SerializeField] private string _teleportAnimationParameterName;

    [SerializeField] private float _teleportSpeed;

    private int _teleportAnimationParameter;

    private bool _teleporting;

    private Vector3 _origin;

    private Vector3 _destination;

    public MMTween.MMTweenCurve TweenCurve;

    private float _startedAt;

    private float _teleportDuration;

    private Phase _phase;

    [SerializeField] private Transform ballTeleport;
    [SerializeField] private CorgiControllerEx _corgi;

    // protected override void Start()
    // {
    //     base.Start();
    //     _corgi = GetControllerEx();
    // }

    protected override void Initialization()
    {
        base.Initialization();
        _phase = Phase.Idle;
    }

    protected override void InitializeAnimatorParameters()
    {
        RegisterAnimatorParameter(_teleportAnimationParameterName, AnimatorControllerParameterType.Trigger,
            out _teleportAnimationParameter);
    }

    public override void UpdateAnimator()
    {
        MMAnimatorExtensions.UpdateAnimatorBool(_animator, _teleportAnimationParameter, _teleporting,
            _character._animatorParameters);
    }

    private CorgiControllerEx GetControllerEx()
    {
        if (!_corgi)
            _corgi = GetComponent<CorgiControllerEx>();
        return _corgi;
    }

    public void TeleportTo(Vector3 destination, bool instantly)
    {
        _origin = transform.position;
        _destination = destination;
        _startedAt = Time.time;

        _teleportDuration = 0;
        _teleporting = true;
        _phase = instantly ? Phase.InstantTeleporting : Phase.Teleporting;
        
        // _corgi.SetColliderActive(false);
        GetControllerEx().enabled = false;
        ballTeleport.gameObject.SetActive(true);
    }

    public void MoveToDestination()
    {
        if (!_teleporting) return;
        switch (_phase)
        {
            case Phase.Teleporting:
                _teleportDuration = Time.time - _startedAt;
                // float remap = MMMaths.Remap(Vector3.Distance(transform.position, _origin), 0, Vector3.Distance(_origin, _destination),
                //     0f, 1f);
                // float factor = math.max(1 - MMTween.Evaluate(remap, TweenCurve), .1f);

                float factor = MMTween.Evaluate(_teleportDuration, TweenCurve);
                
                // Debug.Log(factor);
                Vector3 pos = Vector3.MoveTowards(transform.position, _destination, Time.deltaTime * _teleportSpeed * factor);
                
                transform.SetPositionAndRotation(pos, transform.rotation);
                // transform.position = Vector3.Lerp(transform.position, _destination, factor * _teleportSpeed * Time.deltaTime);
                // transform.position = Vector3.Lerp(transform.position, _destination, MMTween.Evaluate(remap, TweenCurve));
                break;
            case Phase.InstantTeleporting:
                transform.position = _destination;
                break;
            case Phase.Idle:
                break;
            default:
                break;
        }

        if (transform.position == _destination && _teleporting)
        {
            ReachedEnd();
        }
    }

    private void ReachedEnd()
    {
        _phase = Phase.Idle;
        _teleporting = false;
        
        _corgi.ReachedEndTeleport();
        GetControllerEx().enabled = true;
        // GetComponent<Character>().UnFreeze();
        
        ballTeleport.gameObject.SetActive(false);
    }

    public override void ProcessAbility()
    {
        base.ProcessAbility();
        MoveToDestination();
    }

    private void OnValidate()
    {
        if (!_corgi) _corgi = GetControllerEx();
    }
}