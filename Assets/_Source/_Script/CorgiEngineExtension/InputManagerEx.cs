﻿using Framework.DesignPattern.Singleton;
using MoreMountains.CorgiEngine;
using MoreMountains.NiceVibrations;
using MoreMountains.Tools;
using UnityEngine;


public class InputManagerEx : InputManager
{
    [Tooltip("Remember input from last frame and continue doing it, for debug purpose")]
    [SerializeField] private bool stickyInput;
    
    [SerializeField] private MMTouchAxis moveLeftTouchButton;
    [SerializeField] private MMTouchAxis moveRightTouchButton;
    
    public bool forceDesktopModeInEditor;

    public bool invertHorizontalControl;


    protected override void Start()
    {
#if UNITY_EDITOR
        if (forceDesktopModeInEditor)
        {
            ForcedMode = InputForcedMode.Desktop;
        }
#endif
        
        Initialization();
    }
    

    public void Vibrate()
    {
        MMVibrationManager.Haptic(HapticTypes.Selection);
    }
    
    
    
}