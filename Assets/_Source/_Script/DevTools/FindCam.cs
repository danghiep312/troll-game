﻿using System;
using UnityEngine;


[ExecuteInEditMode]
public class FindCam : MonoBehaviour
{
    public Canvas canvas;

    [ExecuteInEditMode]
    private void Update()
    {
        // Debug.Log("Run here");
        if (canvas.worldCamera == null)
        {
            canvas.worldCamera = FindObjectOfType<Camera>();
        }
    }
}