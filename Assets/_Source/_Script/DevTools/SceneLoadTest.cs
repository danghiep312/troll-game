﻿using System;
using Framework.DesignPattern.Singleton;
using Sirenix.OdinInspector;
using UnityEngine;

public class SceneLoadTest : Singleton<SceneLoadTest>
{
    [Button]
    public void LoadScene(string key)
    {
        SceneLoadingMachine.LoadScene(key, false);
    }
    
    [Button]
    public void ActivateScene(string key)
    {
        SceneLoadingMachine.ActivateScene(key);
    }
    
    
    private bool GetInput(KeyCode keyCode)
    {
        return Input.GetKeyDown(keyCode);
    }
}