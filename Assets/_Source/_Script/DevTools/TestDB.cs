﻿
    using BansheeGz.BGDatabase;
    using Sirenix.OdinInspector;
    using UnityEngine;

    public class TestDB : MonoBehaviour
    {
        private GameData gameData;
        public BGEntityGo entityGo;
        
        [Button]
        public void TestGetDb()
        {
            var entity = entityGo.Entity;
            gameData = GameData.GetEntity(0);
            Debug.Log("Life: " + gameData.life);
            
            gameData.life--;
            gameData.Set("life", gameData.life);
            BGRepo.I.Save();
        }
    }
