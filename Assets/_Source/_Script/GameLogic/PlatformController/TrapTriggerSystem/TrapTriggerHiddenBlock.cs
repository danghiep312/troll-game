﻿using UnityEngine;

public class TrapTriggerHiddenBlock : TrapTrigger
{
    [SerializeField]
    [Tooltip("Only activate if the activator is in this area")]
    protected Collider2D activationArea;

    private Collider2D[] _filterResult;

    public ContactFilter2D contactFilter;

    protected override void Initialize()
    {
        // contactFilter = activationArea
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag("Player")) return;
        _filterResult = new Collider2D[10];
        int hitCount = col.GetContacts(contactFilter, _filterResult);
        foreach (Collider2D hit in _filterResult)
        {
            if (hit == activationArea)
            {
                base.OnTriggerEnter2D(col);
                return;
            }
        }        
    }
}