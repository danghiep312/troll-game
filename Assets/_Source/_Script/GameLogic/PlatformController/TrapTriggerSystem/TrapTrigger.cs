﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class TrapTrigger : TrapTriggerBase
{
    [SerializeField] protected UnityEvent onEnterTrigger;
    
    protected override bool ProcessActivation(GameObject activator)
    {
        if (numberOfActivation <= 0 && !_unlimitedActivation) return false;
        numberOfActivation--;
        onEnterTrigger?.Invoke();
        return true;
    }

    private void OnDrawGizmos()
    {
    }

    protected override bool ProcessActivationExit(GameObject activator)
    {
        return default;
    }
}