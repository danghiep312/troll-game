﻿using MoreMountains.CorgiEngine;
using UnityEngine;
using UnityEngine.Serialization;

public class Teleporter : TrapTriggerBase
{
    public Transform destination;

    public bool freezeCharacter;

    public bool resetMovementStateOnTeleport;

    [SerializeField] private bool destinationRelativeToLocal;

    private Vector3? _destinationWorldPos;

    [SerializeField] private bool instantTeleport;

    private Vector3 GetDestination()
    {
        return _destinationWorldPos ?? destination.position;
    }

    protected override void Initialize()
    {
        base.Initialize();
        _destinationWorldPos = destination.position;
    }

    protected override bool ProcessActivation(GameObject activator)
    {
        activationFeedback.transform.position = activator.transform.position;
        activationFeedback?.PlayFeedbacks();

        float scaleDuration = .3f;
        activator.GetComponent<CharacterScaleChanger>().BeginScale(Vector3.zero, scaleDuration, false);
        // Util.Delay(scaleDuration,
        //     () => { activator.GetComponent<CharacterTeleport>().TeleportTo(GetDestination(), instantTeleport); });

        activator.GetComponent<CharacterTeleport>().TeleportTo(GetDestination(), instantTeleport);
        if (freezeCharacter)
        {
            activator.GetComponent<Character>().Freeze();
        }

        return true;
    }

    protected override bool ProcessActivationExit(GameObject activator)
    {
        ActivationExitFeedback?.PlayFeedbacks();
        return true;
    }
}