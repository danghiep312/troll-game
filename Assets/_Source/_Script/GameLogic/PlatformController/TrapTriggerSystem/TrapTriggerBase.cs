﻿using System.Collections;
using MoreMountains.Feedbacks;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;


public abstract class TrapTriggerBase : MonoBehaviour
{
    public LayerMask targetLayerMask;

    [Tooltip("Set to -1 for infinite activation")] [SerializeField]
    protected int numberOfActivation;

    [SerializeField] protected bool disableOnTrigger;

    [SerializeField] protected float delayDuration;

    [Tooltip("Feedback when activation begin")] [SerializeField]
    protected MMF_Player activationBeginFeedback;

    [Tooltip("Feedback when activation success")] [SerializeField]
    protected MMF_Player activationFeedback;

    [Tooltip("Feedback when activation exit")]
    protected MMF_Player ActivationExitFeedback;

    protected int _numberOfActivationLeft;

    protected int _runningCoroutinesCount;

    protected bool _unlimitedActivation;

    protected bool _activatedOnLastEnter;

    protected virtual void Start()
    {
        _numberOfActivationLeft = numberOfActivation;
        _unlimitedActivation = numberOfActivation == -1;
        
        Initialize();
    }

    protected virtual void Initialize()
    {
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        GameObject activator = col.gameObject;
        // Debug.Log(col.gameObject.layer + " " + targetLayerMask.value);
        if (!activator.layer.Equals((int)math.log2(targetLayerMask.value)) || !CanActivate()) return;
        // Debug.Log("TrapTriggerBase.OnTriggerEnter2D");
        if (disableOnTrigger)
        {
            gameObject.SetActive(false);
        }
        activationBeginFeedback?.PlayFeedbacks();
        ProcessActivation(activator);
    }


    protected abstract bool ProcessActivation(GameObject activator);
    
    protected virtual bool CanActivate()
    {
        return _numberOfActivationLeft > 0 || _unlimitedActivation;
    }

    protected void OnTriggerExit2D(Collider2D col)
    {
        GameObject activator = col.gameObject;
        if (!activator.layer.Equals((int)math.log2(targetLayerMask.value))) return;
        ProcessActivationExit(activator);
    }

    protected abstract bool ProcessActivationExit(GameObject activator);
    
}