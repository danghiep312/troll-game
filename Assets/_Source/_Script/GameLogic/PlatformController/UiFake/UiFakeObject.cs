﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


[Tooltip("Make a trap like UI game")]
public class UiFakeObject : MonoBehaviour
{
    public enum UiType
    {
        Home = 1,
        Retry = 2,
        StageTitle = 3,
        Skip = 4
    }

    [SerializeField] private UiType _uiType;

    [SerializeField] private bool _activateOnStart;

    private Button _imposterButton;
    
    private IGameplayButton _gameplayButton;

    [SerializeField] private TMP_Text _imposterText;

    private void Start()
    {
        _imposterButton = GetComponentInChildren<Button>();
        _gameplayButton = _uiType switch
        {
            UiType.Home => new HomeButton(),
            UiType.Retry => new RetryButton(),
            UiType.Skip => new SkipButton(),
            _ => default
        };

        if (_uiType == UiType.StageTitle)
        {
            _imposterText.text = $"LEVEL {MemoryAccess.GetCurrentLevel()}";
        }
    }

    public void Activate()
    {
        if (_gameplayButton != null) _gameplayButton.Activate();
    }

    private void ReplaceButton(Button target)
    {
        target.SetActive(false);
        _imposterButton.SetActive(true);
    }
}