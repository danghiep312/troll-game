﻿using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Framework.DesignPattern.Singleton;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.ResourceProviders;


public class GamePlatformController : MonoBehaviour, MMEventListener<CorgiEngineEvent>
{
    [SerializeField] private StageObjectManager stageObjectManager;

    [SerializeField] private InputManagerEx inputManagerEx;
    private GameSessionData _sessionData;

    private void Start()
    {
        MMEventManager.RemoveListener(this);
        MMEventManager.AddListener(this);
        
        Setup(new GameSessionData());
    }

    private void OnDestroy()
    {
        MMEventManager.RemoveListener(this);
    }

    public async void Setup(GameSessionData sessionData)
    {
        _sessionData = sessionData;

        int currentLevel = MemoryAccess.GetCurrentLevel();
        _sessionData.StageId = currentLevel;

        stageObjectManager = (await $"Stage {currentLevel}".GenerateAsset()).GetComponent<StageObjectManager>();

        DarkZoneEffect.Instance.Open();
    }


    public void OnMMEvent(CorgiEngineEvent eventType)
    {
        Debug.Log("On Event: " + eventType.EventType);
        switch (eventType.EventType)
        {
            case CorgiEngineEventTypes.LevelStart:
                OnLevelStart();
                break;
            case CorgiEngineEventTypes.LevelComplete:
                OnLevelComplete();
                break;
            case CorgiEngineEventTypes.PlayerDeath:
                OnPlayerDeath();
                break;
            default:
                break;
        }
    }

    private void OnLevelStart()
    {
        inputManagerEx.InputDetectionActive = true;

        stageObjectManager = FindObjectOfType<StageObjectManager>();
    }

    private void OnLevelComplete()
    {
        _sessionData.IsWin = true;
        inputManagerEx.InputDetectionActive = false;

        stageObjectManager.SetStateCharacter(false);
        NextLevelProcess();
    }

    private async void OnPlayerDeath()
    {
        _sessionData.IsWin = false;
        
        LifeManager.UseLife();
        
        MMEventManager.RemoveListener(this);
        await UniTask.Delay(TimeSpan.FromSeconds(1f));
        ChangeScene();
    }

    public void ChangeScene()
    {
        DarkZoneEffect.Instance.Close().OnComplete(() => SceneLoadingMachine.LoadScene("Retry", true).Forget());
    }

    public async void NextLevelProcess()
    {
        // MemoryAccess.SetCurrentLevel(MemoryAccess.GetCurrentLevel() + 1);
        var sceneInstance = await SceneLoadingMachine.LoadScene("Main", false);
        DarkZoneEffect.Instance.Close(stageObjectManager.GetPlayerPosition()).OnComplete(nextLevel);

        return;

        async void nextLevel()
        {
            await sceneInstance.ActivateAsync();
            await UniTask.Delay(TimeSpan.FromSeconds(.5f));
            DarkZoneEffect.Instance.Open();
        }
    }

    public class GameSessionData
    {
        public int StageId;
        public bool IsWin;
        public float TimePlayedSec;

        public GameSessionData(int stageId)
        {
            StageId = stageId;
        }

        public GameSessionData()
        {
        }
    }
}