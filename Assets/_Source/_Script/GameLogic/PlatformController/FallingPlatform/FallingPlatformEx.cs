﻿using MoreMountains.CorgiEngine;
using UnityEngine;
using UnityEngine.Events;

public class FallingPlatformEx : FallingPlatform
{
    protected enum Phase
    {
        Idle = 0,
        Falling = 1
    }

    [SerializeField] protected float _gravityScale;

    [SerializeField] private UnityEvent _onFallBegin;

    private Phase _phase;

    protected float GetPlatformTopY()
    {
        return 0f;
    }

    public void FallInstantly()
    {
    }

    public override void OnTriggerStay2D(Collider2D col)
    {
        base.OnTriggerStay2D(col);
        // if (_timer <= 0f) _phase = Phase.Falling;
    }

    protected override void OnTriggerExit2D(Collider2D col)
    {
        base.OnTriggerExit2D(col);
    }

    public void StartShaking()
    {
        _shaking = true;
    }

    protected override void FixedUpdate()
    {
        if (_shaking)
        {
            _timer -= Time.deltaTime;
            if (_timer < 0)
            {
                _phase = Phase.Falling;
                _shaking = false;
            }
        }
        
        if (_phase == Phase.Falling)
        {
            _timer -= Time.fixedDeltaTime;
            FallSpeed += _gravityScale * Mathf.Abs(_timer);
        }

        if (_timer < 0 && _phase == Phase.Falling)
        {
            _newPosition = new Vector2(0, -FallSpeed * Time.deltaTime);

            transform.Translate(_newPosition, Space.World);

            if (transform.position.y < _bounds.min.y - 20f)
            {
                DisableFallingPlatform();
            }
        }
    }
}