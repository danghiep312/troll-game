﻿
    using UnityEngine;

    public class ChangeColorCam : MonoBehaviour
    {
        [SerializeField]
        private Camera _cam;

        [SerializeField]
        private ThemeData _themeData;

        private void Start()
        {
            GetThemeColor();
        }

        private void GetThemeColor()
        {
            _cam.backgroundColor = _themeData.backgroundColor;
        }
    }
