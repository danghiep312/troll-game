﻿using DG.Tweening;
using UnityEngine;

public class TrapRotateController : MonoBehaviour
{
    [SerializeField]
    [Header("Trap Parameters")]
    private float _targetZAngle;

    [SerializeField]
    private float _angularRotationSpeed;

    [Header("Trap Components")]
    [SerializeField]
    private GameObject _trapRotateObject;

    private float _currentTrapZRotation;

    private bool _isActivated;

    private void Start()
    {
        _isActivated = false;
        
    }

    public void TriggerTrap()
    {
        // If the trap is already activated, return
        if (_isActivated) return;
        _isActivated = true;
        // GetComponent<Animator>().Play("Fall Rotate");
        _trapRotateObject.transform.DORotate(Vector3.forward * _targetZAngle, _targetZAngle / _angularRotationSpeed).SetEase(Ease.Linear);
    }
}