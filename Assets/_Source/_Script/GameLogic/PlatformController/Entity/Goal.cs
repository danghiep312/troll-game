﻿using System;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using UnityEngine;

public class Goal : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private float winAnimDuration;
    [SerializeField] private MMF_Player activateFeedback;

    private bool _isTriggered;

    private void OnTriggerEnter2D(Collider2D col)
    {
        // if (_isTriggered) return;
        Debug.Log(col.gameObject.tag);
        if (col.CompareTag("Player"))
        {
            col.gameObject.SetActive(false);
            _isTriggered = true;
            PlayAnimation();
            activateFeedback?.PlayFeedbacks();
            // MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.LevelComplete));
            Util.Delay(winAnimDuration, () =>
            {
                MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.LevelComplete));    
            });
        }
    }

    private void PlayAnimation()
    {
        animator.SetTrigger("activate");
    }

    [Button]
    public void TestAnimation()
    {
        animator.SetTrigger("activate");
        
    }

    private void OnValidate()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
    
        if (activateFeedback == null)
        {
            activateFeedback = GetComponent<MMF_Player>();
        }
    }
}

public enum AnimMap : int
{
    CONDITION = 0,
}