﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class Block : MonoBehaviour
{
    protected bool _bounceOnHit;

    protected int _numberOfAllowedHits;

    protected int _numberOfAllowedHitsLeft;

    [SerializeField] private SpriteRenderer _renderer;

    [SerializeField] [Header("Animation")] protected float _bouncePower = 0.1f;

    [SerializeField] protected float _bounceDuration = 0.1f;

    protected void Start()
    {
    }

    protected void OnCollisionEnter2D(Collision2D col)
    {
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {

        if (!_bounceOnHit) return;
            
        BounceInPlace();
    }

    protected void OnColliding(Collider2D col)
    {
    }

    [Button]
    public void BounceInPlace()
    {   
        // _renderer.transform.DOShakePosition(_bounceDuration, _bouncePower, 1, 0f, true, false, ShakeRandomnessMode.Harmonic);
        _renderer.transform.DOLocalMoveY(_bouncePower, _bounceDuration).OnComplete(() =>
        {
            _renderer.transform.DOLocalMoveY(0, _bounceDuration);
        });
    }

    private void OnValidate()
    {
        _renderer = GetComponentInChildren<SpriteRenderer>();
    }
}