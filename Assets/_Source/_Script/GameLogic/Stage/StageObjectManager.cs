﻿using System;
using MoreMountains.CorgiEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
#endif
using UnityEngine;
using UnityEngine.Tilemaps;

public class StageObjectManager : MonoBehaviour
{
    public bool isDebugging;

    [SerializeField] private LevelManagerEx levelManager;

    [SerializeField] private CorgiController playerCharacter;


    [Header("Component change color")] [SerializeField]
    private Tilemap tileMap;

    [SerializeField] private Tilemap bgTileMap;

    [SerializeField] private Camera cam;

    public Camera Camera => cam;
    public LevelManager Level => levelManager;
    public CorgiController PlayerCharacter => playerCharacter;

    private void Start()
    {
        if (isDebugging) return;
        foreach (var sr in GetComponentsInChildren<SpriteRenderer>())
        {
            if (sr.gameObject.name.Contains("Debug")) sr.gameObject.SetActive(false);
        }
    }

    public void SetStateCharacter(bool status)
    {
        playerCharacter.SetActive(status);
    }


    public int? GetId()
    {
        // levelManager
        return default;
    }

    public void Setup()
    {
        // TODO: setup color
    }

    public Vector3 GetPlayerPosition()
    {
        return playerCharacter.transform.position;
    }

#if UNITY_EDITOR

    private void OnValidate()
    {
        if (cam == null)
        {
            cam = GetComponentInChildren<Camera>();
        }

        if (levelManager == null)
        {
            levelManager = GetComponentInChildren<LevelManagerEx>();
        }

        if (playerCharacter == null)
        {
            playerCharacter = GetComponentInChildren<CorgiController>();
        }

        if (levelManager != null)
        {
            levelManager.goal = GetComponentInChildren<Goal>();
            if (playerCharacter != null)
            {
                var character = playerCharacter.GetComponentInChildren<Character>();
                if (!levelManager.SceneCharacters.Contains(character)) levelManager.SceneCharacters.Add(character);
            }
        }

        var settings = AddressableAssetSettingsDefaultObject.Settings;

        string assetPath = AssetDatabase.GetAssetPath(gameObject);
        //string directoryPath = System.IO.Path.GetDirectoryName(assetPath);
        string guid = AssetDatabase.AssetPathToGUID(assetPath);
        AddressableAssetEntry entry = settings.FindAssetEntry(guid);
        if (entry != null)
        {
            if (entry.parentGroup == settings.groups[3])
            {
                entry.address = gameObject.name;
            }
        }
    }
#endif
}