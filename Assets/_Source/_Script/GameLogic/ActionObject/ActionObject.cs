﻿using UnityEngine;
using UnityEngine.Events;

public class ActionObject : MonoBehaviour
{
    [SerializeField] protected UnityEvent _onRunAction;
    
    public virtual void Run()
    {
        _onRunAction?.Invoke();
    }
    
}