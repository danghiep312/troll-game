﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class DelayActionCustom : ActionObject
{
    [SerializeField] protected float _delayDuration;

    public override async void Run()
    {
        await UniTask.Delay(TimeSpan.FromSeconds(_delayDuration));
        
        _onRunAction?.Invoke();
    }
}