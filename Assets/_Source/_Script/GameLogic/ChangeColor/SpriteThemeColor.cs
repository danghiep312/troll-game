using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteThemeColor : MonoBehaviour
{
    [SerializeField]
    private ThemeData theme;

    [SerializeField]
    private ThemeColorType colorType = ThemeColorType.Ground;

    private SpriteRenderer _spriteRenderer;

    private Image _image;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _image = GetComponent<Image>();

        if (_spriteRenderer != null)
        {
            _spriteRenderer.color = GetColor(colorType);
        }
        else if (_image != null)
        {
            _image.color = GetColor(colorType);
        }
    }
    
    private Color GetColor(ThemeColorType type)
    {
        return type == ThemeColorType.Saw ? theme.sawColor : theme.tileMapColor;
    }
}


public enum ThemeColorType
{
    Ground = 1,
    Obstacle = 2,
    Saw = 3
}