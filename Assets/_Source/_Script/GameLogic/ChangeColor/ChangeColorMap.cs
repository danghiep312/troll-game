﻿
using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ChangeColorMap : MonoBehaviour
{
    [SerializeField]
    private Tilemap decoTileMap;

    [SerializeField]
    private ThemeData themeData;

    private void Start()
    {
        if (decoTileMap != null)
        {
            decoTileMap.color = themeData.tileMapColor;
        }
    }

    private void GetThemeColor()
    {
    }

    private void OnValidate()
    {
        if (decoTileMap == null)
        {
            decoTileMap = GetComponent<Tilemap>();
        }
    }
}