﻿#if UNITY_EDITOR
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;
#endif

public class PreBuildHandle : IPreprocessBuildWithReport
{
#if UNITY_EDITOR
    public int callbackOrder => 2;

    public void OnPreprocessBuild(BuildReport report)
    {
        // AddressableAssetSettings.CleanPlayerContent(); // Clean everything
        // Common.Log("LOG::: Start Build addressable");
        //
        // // There is issue where Addressable in batch mode. Ask for confirm scene modified and stop build.
        // // You can check log and see that addressable build time = 0
        // AssetDatabase.SaveAssets();
        // AddressableAssetSettings.BuildPlayerContent();
        // Common.Log("LOG::: Build addressable Done");
    }
#endif
}