﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

public class SceneLoadingMachine
{
    public static Dictionary<string, SceneInstance> LoadedScenes = new Dictionary<string, SceneInstance>();

    /// <summary>
    /// Load scene from anywhere
    /// </summary>
    /// <param name="sceneKey"> addressable key of scene </param>
    /// <param name="activateOnLoad"> active scene after load done </param>
    public static async UniTask<SceneInstance> LoadScene(string sceneKey, bool activateOnLoad)
    {
        // if (LoadedScenes.TryGetValue(sceneKey, out SceneInstance sceneInstance))
        // {
        //     sceneInstance.ActivateAsync();
        //     return;
        // }
        Debug.Log("Load Scene: " + sceneKey);
        AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync(sceneKey, LoadSceneMode.Single, activateOnLoad);
        await handle;
        return handle.Result;
    }
    
    public static void ActivateScene(string sceneKey)
    {
        Debug.Log("Activate Scene: " + sceneKey);
        if (LoadedScenes.TryGetValue(sceneKey, out SceneInstance sceneInstance))
        {
            sceneInstance.ActivateAsync();
        }
    }
    
    public static void UnloadScene(string sceneKey)
    {
        if (LoadedScenes.TryGetValue(sceneKey, out SceneInstance sceneInstance))
        {
            Addressables.UnloadSceneAsync(sceneInstance);
            LoadedScenes.Remove(sceneKey);
        }
    }
    
    public static void UnloadAllScenes(string exceptSceneKey)
    {
        var exists = LoadedScenes.TryGetValue(exceptSceneKey, out SceneInstance exceptSceneInstance);
        foreach (var scene in LoadedScenes.Where(scene => scene.Key != exceptSceneKey))
        {
            Addressables.UnloadSceneAsync(scene.Value);
        }
        // LoadedScenes.Clear();
        
        // if (exists) LoadedScenes.TryAdd(exceptSceneKey, exceptSceneInstance);
    }
}