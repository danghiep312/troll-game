﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class DefaultValue
{
    // [HideLabel]
    // [EnumToggleButtons]
    // public DataType type;
    // public string key;
    //
    // [ShowIf("type", DataType.String)]
    // public string stringValue;
    //
    // [ShowIf("type", DataType.Int)]
    // public int intValue;
    //
    // [ShowIf("type", DataType.Float)]
    // public float floatValue;
    //
    // public void DoSave()
    // {
    //     if (PlayerPrefs.HasKey(key)) return;
    //     if (type == DataType.String)
    //         MemoryAccess.Set(key, stringValue);
    //     else if (type == DataType.Int)
    //         MemoryAccess.Set(key, intValue);
    //     else if (type == DataType.Float)
    //         MemoryAccess.Set(key, floatValue);
    // }
    //
    // public T GetValue<T>()
    // {
    //     if (type == DataType.String)
    //         return (T) Convert.ChangeType(MemoryAccess.Get(key, stringValue), typeof(T));
    //     if (type == DataType.Int)
    //         return (T) Convert.ChangeType(MemoryAccess.Get(key, intValue), typeof(T));
    //     if (type == DataType.Float)
    //         return (T) Convert.ChangeType(MemoryAccess.Get(key, floatValue), typeof(T));
    //     return default;
    // }
}

// public enum DataType
// {
//     Int, Float, String
// }