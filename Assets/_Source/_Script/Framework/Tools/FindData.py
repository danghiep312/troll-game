﻿from pathlib import Path
import sys

# D:\Unity Project\Test\Assets\ExportedProject\Assets\MonoBehaviourD:\Unity Project\Test\Assets\ExportedProject\Assets\Resources\sounds\sfx
directory_in_str = "D:\\Unity Project\\Test\\Assets\\ExportedProject\\Assets"
# directory_in_str = sys.argv[1]

find_list = ["5b009423134479b51df14c3c33ef47eb"]

pathlist = Path(directory_in_str).glob('**/*.*')
for path in pathlist:
     # because path is object not string
    try:
        path_in_str = str(path)
        f = open(path_in_str, "r")
        contents = f.read()
        # if contents.find("af9901fa9eddce443a7c82425840f43a") != -1:
        #     print(path_in_str)
        for find in find_list:
            if contents.find(find) != -1:
                print(path_in_str)
        f.close()
    except:
        continue