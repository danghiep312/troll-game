﻿using System;
using DG.Tweening;
using Framework.DesignPattern.Singleton;
using Sirenix.OdinInspector;
using UnityEngine;

public class DarkZoneEffect : Singleton<DarkZoneEffect>
{
    public DarkZoneSetting closeSetting;
    public DarkZoneSetting openSetting;


    [SerializeField] private Transform mask;

    private void Start()
    {
        // gameObject.SetActive(false);
    }

    [Button]
    public Sequence Close(Vector3 position = default, DarkZoneSetting overrideSetting = null)
    {
        mask.position = position;
        return DarkZoneAnimation(overrideSetting ?? closeSetting);
    }

    [Button]
    public Sequence Open(Vector3 position = default, DarkZoneSetting overrideSetting = null)
    {
        mask.position = position;
        return DarkZoneAnimation(overrideSetting ?? openSetting);
    }

    Sequence DarkZoneAnimation(DarkZoneSetting setting)
    {
        Sequence sequence = DOTween.Sequence();

        mask.localScale = setting.InitValue;
        gameObject.SetActive(true);
        sequence.Append(mask.DOScale(setting.Target, setting.Duration).SetEase(setting.Ease).SetDelay(setting.Delay));

        return sequence;
    }

    public void SetPositionMask(Vector3 position)
    {
        mask.position = position;
    }
}

[System.Serializable]
public class DarkZoneSetting
{
    public Vector3 InitValue;
    public Vector3 Target;
    public float Duration;
    public Ease Ease;
    public float Delay;
}